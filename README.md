# WEB Automation Cypress

Web automation using Cypress framework

## Getting started

To start your project you need to run first commands in your project folder:
/home/user/project_folder/ npm init

Then you need to run command:
/home/user/project_folder/ npm install cypress --save-dev

Then you need to run command:
/home/user/project_folder/ node ./node_modules/cypress/bin/cypress open

after that you can run only: npx cypress open to open cypress UI to execute your test file.